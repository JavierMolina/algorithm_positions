def to_up_down(m):
    if (m % 2 == 0):
        return 'U'
    else:
        return 'D'


def to_left_rigth(n):
    if (n % 2 == 0):
        return 'L'
    else:
        return 'R'


def get_position(n, m):
    print(f'{n} {m}')
    if (n > m):
        result = to_up_down(m)
    else:
        result = to_left_rigth(n)
    print(f'{result}\n')


get_position(1, 1)
get_position(2, 2)
get_position(3, 1)
get_position(3, 3)
